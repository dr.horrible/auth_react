import { Button, Stack, TextField, Typography } from '@mui/material'
import { useForm } from 'react-hook-form'
import { auth } from '@/api'
import { useNavigate } from 'react-router'
import type { SetIsAuthProp, AuthRequestParams } from '@/@types'

export const Auth = ({ setIsAuth }: SetIsAuthProp) => {
  const navigate = useNavigate()
  const { register, handleSubmit } = useForm<AuthRequestParams>()

  const onSubmit = (data: AuthRequestParams) => {
    auth(data).then(({ data }) => {
      localStorage.setItem('token', data.token)
      setIsAuth(true)

      navigate('/')
    })
  }

  return (
    <Stack spacing={4}>
      <Typography variant='h2' component='h1'>
        Кто ты, пирожок?
      </Typography>
      <form onSubmit={handleSubmit(onSubmit)}>
        <Stack spacing={2}>
          <TextField {...register('login')} label='Логин' />
          <TextField {...register('password')} label='Пароль' type='password' />
          <Button variant='contained' size='large' type='submit'>
            Отправить
          </Button>
        </Stack>
      </form>
    </Stack>
  )
}
