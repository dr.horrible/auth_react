import { useEffect, useState } from 'react'
import type { Post, SetIsAuthProp } from '@/@types'
import { getPosts } from '@/api/posts'
import { jwtDecode } from '@/utils'
import { Button, Card, CardContent, Stack, Typography } from '@mui/material'

export const User = ({ setIsAuth }: SetIsAuthProp) => {
  const [name, setName] = useState('')
  const [posts, setPosts] = useState<Post[]>([])

  const token = localStorage.getItem('token')

  const handlerClick = () => {
    getPosts().then(({ data }) => setPosts(data))
  }

  useEffect(() => {
    if (token) {
      const data = jwtDecode(token)

      setName(data.name)
    }
  }, [token])

  return (
    <Stack spacing={3} sx={{ height: '100vh', textAlign: 'center' }}>
      <Button
        onClick={() => {
          localStorage.clear()
          setIsAuth(false)
        }}
      >
        Разлогиниться
      </Button>
      <Typography variant='h3' component='h1'>
        Хай, {name}
      </Typography>
      <Button onClick={handlerClick}>Показать посты</Button>
      <Stack spacing={2}>
        {posts.length > 0 &&
          posts.map((item) => (
            <Card sx={{ maxWidth: 350 }} key={item.id}>
              <CardContent>
                <Typography gutterBottom variant='h5' component='div'>
                  {item.title}
                </Typography>
                <Typography variant='body2' color='text.secondary'>
                  {item.body}
                </Typography>
              </CardContent>
            </Card>
          ))}
      </Stack>
    </Stack>
  )
}
