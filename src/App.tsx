import { Navigate, Route, Routes } from 'react-router'
import './App.css'
import { User, Auth } from './modules'
import { useEffect, useState } from 'react'

function App() {
  const [isAuth, setIsAuth] = useState(false)

  const token = localStorage.getItem('token')

  useEffect(() => {
    setIsAuth(!!token)
  }, [token])

  return (
    <Routes>
      <Route index element={!token && !isAuth ? <Navigate to='/auth' replace /> : <User setIsAuth={setIsAuth} />} />
      <Route path='/auth' element={!token && !isAuth ? <Auth setIsAuth={setIsAuth} /> : <Navigate to='/' replace />} />
    </Routes>
  )
}

export default App
