export interface SetIsAuthProp {
  setIsAuth: React.Dispatch<React.SetStateAction<boolean>>
}
