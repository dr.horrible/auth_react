export interface AuthRequestParams {
  login: string
  password: string
}

export interface AuthResponseData {
  token: string
}

export interface Post {
  id: number
  title: string
  body: string
}
