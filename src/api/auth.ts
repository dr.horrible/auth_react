import { AuthRequestParams, AuthResponseData } from '@/@types/api'
import { baseUrl } from '@/utils/contsants'
import axios, { AxiosResponse } from 'axios'

export const auth = (params: AuthRequestParams): Promise<AxiosResponse<AuthResponseData>> =>
  axios.post(`${baseUrl}auth`, params)
