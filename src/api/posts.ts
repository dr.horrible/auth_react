import { instance } from './instance'

export const getPosts = () => instance.get('/posts')
