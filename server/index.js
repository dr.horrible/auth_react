import express from 'express'
import cors from 'cors'
import bodyParser from 'body-parser'
import jwt from 'jsonwebtoken'
import random from 'random-name'

import POSTS from './mocks/posts.json' assert { type: 'json' }

const app = express()

const HOST = 'localhost'
const PORT = 3001

app.use(cors({ credentials: true, origin: true }))

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))

app.post('/auth', (req, res) => {
  res.send({
    token: jwt.sign({ iat: new Date().getTime(), name: random.first() }, 'shhhhh'),
  })
})

app.get('/posts', (req, res, next) => {
  if (req.headers.authorization) {
    res.send(POSTS)
  } else {
    res.send('А нифига')
  }
})

app.listen(PORT, HOST, () => {
  console.log(`Server running at http://${HOST}:${PORT}/`)
})
